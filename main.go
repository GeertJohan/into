package main

import (
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	unarr "github.com/gen2brain/go-unarr"
	"github.com/h2non/filetype"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: into <path/to/file.tar.gz>")
		os.Exit(1)
	}
	filename := os.Args[1]

	fmt.Println("Warning: Changes you make will not be saved to the compressed/archive file.")
	fmt.Println("Exit when you have completed inspecting the files.")

	tmpdir := filepath.Join(os.TempDir(), "into", fmt.Sprintf("%s-%s-%s", filepath.Base(filename), time.Now().Format(`2006-01-02-15-04-05`), RandomString(5)))
	err := os.MkdirAll(tmpdir, 0775)
	if err != nil {
		fmt.Printf("error: could not create tmp dir: %v\n", err)
		os.Exit(1)
	}
	// TODO: better cleanup procedure, doesn't respect os.Exit()
	defer func() {
		fmt.Printf("Removing tmp dir %s\n", tmpdir)
		err := os.RemoveAll(tmpdir)
		if err != nil {
			fmt.Printf("error: could not clean up tmp dir: %v\n", err)
			os.Exit(1)
		}
	}()

	// detect filetype, if gzip, first decompress.
	// Open a file descriptor
	file, err := os.Open(filename)
	if err != nil {
		fmt.Printf("error: could not open file (%s): %v\n", filename, err)
		os.Exit(1)
	}

	// read first 261 bytes from file to detect the file type
	head := make([]byte, 261)
	_, err = file.Read(head)
	if err != nil && err != io.EOF {
		fmt.Printf("error: could not read from file (%s): %v\n", filename, err)
		os.Exit(1)
	}
	_, err = file.Seek(0, 0)
	if err != nil {
		fmt.Printf("error: failed to reset file offset: %v\n", err)
		os.Exit(1)
	}

	// detect the file type based on first bytes
	kind, err := filetype.Match(head)
	if err != nil {
		fmt.Printf("error: unknown file type for (%s): %s\n", filename, err)
		return
	}
	var archiveReader io.Reader
	if kind.MIME.Value == "application/gzip" {
		archiveReader, err = gzip.NewReader(file)
		if err != nil {
			fmt.Printf("error: failed to setup gzip reader: %v\n", err)
		}
	} else {
		archiveReader = file
	}

	archive, err := unarr.NewArchiveFromReader(archiveReader)
	if err != nil {
		fmt.Printf("error: could not open archive: %v\n", err)
		os.Exit(1)
	}
	err = archive.Extract(tmpdir)
	if err != nil {
		fmt.Printf("error: could not extract into tmp dir: %v\n", err)
		os.Exit(1)
	}
	shell := os.Getenv("SHELL")
	if len(shell) == 0 {
		fmt.Println("error: missing SHELL environment variable.")
		os.Exit(1)
	}
	shellPath, err := exec.LookPath(shell)
	if err != nil {
		fmt.Printf("error: could not lookup shell executable (%s): %v\n", shell, err)
		os.Exit(1)
	}
	cmd := exec.Command(shellPath)
	cmd.Dir = tmpdir
	cmd.Env = os.Environ()
	cmd.Env = append(cmd.Env, "INTO="+tmpdir)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Start()
	if err != nil {
		fmt.Printf("error: failed to start shell: %v\n", err)
		os.Exit(1)
	}
	err = cmd.Wait()
	if err != nil {
		fmt.Printf("Shell exited with error: %v\n", err)
		// graceful
	}
}
